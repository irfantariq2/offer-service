# Description
Offer service based on Spring Boot

# Prerequisites
## To Build
- Java 8

# How to Build
The project can be built using the included gradle wrapper by running the following command:
```
./gradlew build
```

# How to Run
The service can be run from source or from the compiled JAR file:

java -jar build/libs/offers-rest-service-1.0.0.jar

# Create an offer
curl command to create offer will return id:

curl --header "Content-Type: application/json" --request POST --data '{"description":"des1","amount":13.33,"currency":"USD","expiryInSeconds":1000 }' http://localhost:8080/offers

# Find an offer
curl command to query the service with offer id, if offer expired the will return HTTP 410:

curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/offers/1

# Cancel an offer

curl command to cancel or delete an offer

curl -X "DELETE" http://localhost:8080/offers/1
