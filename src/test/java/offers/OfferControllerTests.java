package offers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OfferControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createOfferMethodReturnID() throws Exception {

        String requestBodyOffer1 = "{\"description\":\"des1\",\"amount\":13.33,\"currency\":\"USD\",\"expiryInSeconds\":1000 }";

        String requestBodyOffer2 = "{\"description\":\"des2\",\"amount\":20.00,\"currency\":\"GBP\",\"expiryInSeconds\":0 }";

        this.mockMvc.perform(post("/offers").content(requestBodyOffer1).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));

        this.mockMvc.perform(post("/offers").content(requestBodyOffer2).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"));
    }

    @Test
    public void findCreatedOfferMethodReturnOffer() throws Exception {

        this.mockMvc.perform(get("/offers/{id}", "1" ))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("des1"))
                .andExpect(jsonPath("$.amount").value("13.33"))
                .andExpect(jsonPath("$.currency").value("USD"))
                .andExpect(jsonPath("$.expiryInSeconds").value("1000"));
    }

    @Test
    public void findExpiredOfferQueryMethodReturnNotFound() throws Exception {

        this.mockMvc.perform(get("/offers/{id}", "1" ))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value("des1"));

        this.mockMvc.perform(get("/offers/{id}", "2" ))
                .andDo(print()).andExpect(status().isGone());
    }

    @Test
    public void offerCancellationBeforeExpiryMethodReturnOffer() throws Exception {

            this.mockMvc.perform(delete("/offers/{id}", "1"))
                .andDo(print()).andExpect(status().isOk());

        this.mockMvc.perform(get("/offers/{id}", "1" ))
                .andDo(print()).andExpect(status().isNotFound());

    }

}
