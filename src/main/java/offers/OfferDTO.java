package offers;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Currency;

public class OfferDTO extends OfferPojo {

    private final Timestamp creationTimeStamp;

    public OfferDTO(String description, BigDecimal amount, Currency currency,
                        Integer expiryInSeconds, Timestamp creationTimeStamp) {
        super(description, amount, currency, expiryInSeconds);
        this.creationTimeStamp = creationTimeStamp;
    }

    public boolean hasExpired() {
        return (creationTimeStamp.getTime() + this.getExpiryInSeconds() * 1000)  < System.currentTimeMillis();
    }

}
