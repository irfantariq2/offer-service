package offers;

import java.math.BigDecimal;
import java.util.Currency;

public class OfferPojo {

    private String description;
    private BigDecimal amount;
    private Currency currency;
    private Integer expiryInSeconds;

    public OfferPojo() {}

    public OfferPojo(String description, BigDecimal amount, Currency currency,
                     Integer expiryInSeconds) {
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.expiryInSeconds = expiryInSeconds;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Integer getExpiryInSeconds() {
        return expiryInSeconds;
    }

}
