package offers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OfferController {

    @Autowired
    OfferService service;

    @RequestMapping(value = "/offers", method = RequestMethod.POST)
    public OfferId createOffer(@RequestBody OfferRequest offerRequest) {
        return service.createOffer(offerRequest);
    }

    @RequestMapping(value = "/offers/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> queryOffer(@PathVariable Integer id) {
        OfferDTO offerDTO = service.findOffer(id);
        if (offerDTO == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else if (offerDTO.hasExpired()) {
            return new ResponseEntity<>(null, HttpStatus.GONE);
        } else {
            OfferPojo offerPojo = new OfferPojo(offerDTO.getDescription(),
                    offerDTO.getAmount(), offerDTO.getCurrency(), offerDTO.getExpiryInSeconds());
            return new ResponseEntity<>(offerPojo, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/offers/{id}", method = RequestMethod.DELETE)
    public void cancelOffer(@PathVariable Integer id) {
        service.cancelOffer(id);
    }


}
