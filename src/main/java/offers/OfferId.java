package offers;

public class OfferId {

  private int id;

  public OfferId(){}

  public OfferId(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

}
