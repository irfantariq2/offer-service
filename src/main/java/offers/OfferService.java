package offers;

import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class OfferService {

    private final AtomicInteger counter = new AtomicInteger();
    private Hashtable<Integer, OfferDTO> offerHashtable = new Hashtable<Integer, OfferDTO>();

    public OfferId createOffer(OfferRequest offer){
        counter.incrementAndGet();
        OfferDTO newOffer = new OfferDTO(offer.getDescription(), offer.getAmount(),
                offer.getCurrency(), offer.getExpiryInSeconds(), new Timestamp(System.currentTimeMillis()));
        offerHashtable.put(counter.intValue(), newOffer);
        return new OfferId(counter.intValue());
    }

    public OfferDTO findOffer(Integer offerId) {
        return offerHashtable.get(offerId);
    }

    public void cancelOffer(Integer offerId) {
        offerHashtable.remove(offerId);
    }

}
