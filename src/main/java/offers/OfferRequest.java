package offers;

import java.math.BigDecimal;
import java.util.Currency;

public class OfferRequest extends OfferPojo {

    public OfferRequest() { super();}

    public OfferRequest(String description, BigDecimal amount, Currency currency, Integer expiryInSeconds) {
        super(description, amount, currency, expiryInSeconds);
    }
}
